#include <boost/xpressive/xpressive.hpp>
#include <boost/math/constants/constants.hpp>

#include <iostream>
#include <sstream>
#include <iomanip>
#include <string>
#include <unordered_map>
#include <memory>

using namespace boost::xpressive;

// All code in one file... as in formulation of the problem. You can use Todo list for navigation.
namespace calc_angle {

    typedef double fnum;

    enum ClockType  { Mechanical, Quartz };
    enum ResultType { deg, rad, dms };

    // TODO: Tables declaration
    /** \brief Table values for convert string-values to the model representation.
     */
    class Tables
    {
    public:
        static const Tables& instance();

        ClockType toClockType( const std::string& strType ) const;
        ResultType toResultType( const std::string& strResultType ) const;

    private:
        Tables();
        Tables( const Tables& src ) = delete;
        Tables& operator=( const Tables& rhs ) = delete;

        std::unordered_map< std::string, ClockType> mClockType;
        std::unordered_map< std::string, ResultType> mResultType;
    };

    // TODO: Time declaration
    /** \brief Simple time struct.
     */
    struct Time {
        fnum hour;
        fnum minute;
        std::string type; // it's doesn't matter for algorithm
    };

    // TODO: Angle declaration
    /** \brief Simple angle struct + conversion method.
     */
    struct Angle {
        fnum angle;
        std::string toString( ResultType type = ResultType::deg );
    };

    // TODO: ICalcAngle declaration
    /** \brief Interface to calculation algorithms.
     */
    class ICalcAngle
    {
    public:
        virtual ~ICalcAngle();
        virtual fnum calculate( Time& time ) = 0;
    };

    // TODO: CalcAngleForQClock declaration
    /** \brief Calculation algorithm for quartz clock.
     */
    class CalcAngleForQClock : public ICalcAngle
    {
    public:
        virtual ~CalcAngleForQClock();
        virtual fnum calculate( Time& time );
    protected:
        fnum fixHour( const fnum& h );
    };

    // TODO: CalcAngleForMClock declaration
    /** \brief Calculation algorithm for mechanical clock.
     */
    class CalcAngleForMClock : public CalcAngleForQClock
    {
    public:
        virtual ~CalcAngleForMClock();
        fnum calculate( Time& time );
    };

    // TODO: CalcAngleBetweenHandsModel declaration
    /** \brief Model calculating angle between the clock hands.
     */
    class CalcAngleBetweenHandsModel
    {
    public:
        Time getTime() const;
        void setTime( const Time& time );

        ClockType getClockType() const;
        void setClockType( ClockType type );

        ResultType getResultType() const;
        void setResultType( ResultType type );

        void calulate();
        Angle getAngle() const;

    private:
        Time mTime;
        Angle mAngle;
        ClockType mClockType;
        ResultType mResultType;
    };

    // TODO: Validator declaration
    /** \brief The strategy validation.
     */
    class Validator
    {
    public:
        const static size_t kMaxTimeSize = 8;
        const static size_t kMaxClockTypeSize = 2;
        const static size_t kMaxOutputFormatSize = 3;
        const static size_t kMaxHourValue24 = 23;
        const static size_t kMaxHourValueAMPM = 11;
        const static size_t kMaxMinuteValue = 59;

        bool timeIsValid( const std::string&  rawTime, Time& output );
        bool clockIsValid( const std::string&   rawClock, std::string& output );
        bool formatIsValid( const std::string&  rawFormat, std::string& output  );
    };

    // TODO: CalcAngleBetweenHandsController declaration
    /** \brief Controller for the calculation angle model..
     */
    class CalcAngleBetweenHandsController
    {
    public:
        CalcAngleBetweenHandsController( CalcAngleBetweenHandsModel& model );

        void setTime( const char* rawTime );
        void setKindOfClock( const char* rawType );
        void setOutputFormat( const char* rawFormat );

        void calculate();
        void displayResult() const;

    private:
        std::string mTime;
        std::string mKindOfClock;
        std::string mOutputFormat;
        std::string mResult;

        Validator mValidator;
        CalcAngleBetweenHandsModel& mModel;
    };

    // TODO: CalcAngleBetweenHandsController implementation
    CalcAngleBetweenHandsController::CalcAngleBetweenHandsController( CalcAngleBetweenHandsModel& model )
        : mTime( "" )
        , mKindOfClock( "" )
        , mOutputFormat( "" )
        , mResult( "" )
        , mValidator()
        , mModel( model )
    {
    }

    void CalcAngleBetweenHandsController::setTime(const char* rawTime)
    {
        if ( rawTime != nullptr ) {
            if( strlen( rawTime ) < mTime.max_size() ) {
                mTime = rawTime ;
            }
        }
    }

    void CalcAngleBetweenHandsController::setKindOfClock(const char* rawType)
    {
        if ( rawType!= nullptr ) {
            if( strlen( rawType ) < mKindOfClock.max_size() ) {
                mKindOfClock = rawType ;
            }
        }
    }

    void CalcAngleBetweenHandsController::setOutputFormat(const char* rawFormat)
    {
        if ( rawFormat!= nullptr ) {
            if( strlen( rawFormat ) < mOutputFormat.max_size() ) {
                mOutputFormat = rawFormat ;
            }
        }
    }

    void CalcAngleBetweenHandsController::calculate()
    {
        Time tmpTime;
        std::string tmpClockType, tmpResulType;

        if ( !mValidator.timeIsValid( mTime, tmpTime ) ) {
            mResult = "TimeFormat error!";
            return;
        }
        if ( !mValidator.formatIsValid( mOutputFormat, tmpResulType ) ) {
            mResult = "Output format error!";
            return;
        }
        if ( !mValidator.clockIsValid( mKindOfClock, tmpClockType ) ) {
            mResult = "Clock format error!";
            return;
        }

        const Tables& ref_t = Tables::instance();
        mModel.setTime( tmpTime );
        mModel.setClockType( ref_t.toClockType( tmpClockType ) );
        mModel.setResultType(ref_t.toResultType( tmpResulType ) );

        mModel.calulate();

        mResult = mModel.getAngle().toString( ref_t.toResultType( tmpResulType ) );
    }

    void CalcAngleBetweenHandsController::displayResult() const
    {
        std::cout << mResult;
    }

    // TODO: Angle implementation
    std::string Angle::toString(ResultType type)
    {
        std::string result;
        std::ostringstream ss;

        switch ( type ) {
            case ResultType::deg:
                ss << angle ;
                break;
            case ResultType::rad:
                ss << std::setprecision( 5 ) <<  ( angle * boost::math::constants::pi<fnum>() / 180.0 );
                break;
            case ResultType::dms:
                {
                    fnum deg = std::trunc( angle ),
                           m = trunc( ( angle - deg ) * 60.0 ),
                           s = trunc( ( ( angle - deg ) * 60.0 - m ) * 60.0 );
                    ss << deg << "." << m << "'" << s << "''";
                }
                break;
        }

        result = ss.str();
        return result;
    }

    // TODO: CalcAngleBetweenHandsModel implementation
    void CalcAngleBetweenHandsModel::setTime(const Time& time)
    {
        mTime = time;
    }

    void CalcAngleBetweenHandsModel::setClockType(ClockType type)
    {
        mClockType = type;
    }

    void CalcAngleBetweenHandsModel::setResultType(ResultType type)
    {
        mResultType = type;
    }

    ResultType CalcAngleBetweenHandsModel::getResultType() const
    {
        return mResultType;
    }

    ClockType CalcAngleBetweenHandsModel::getClockType() const
    {
        return mClockType;
    }

    Time CalcAngleBetweenHandsModel::getTime() const
    {
        return mTime;
    }

    void CalcAngleBetweenHandsModel::calulate()
    {
        typedef std::unique_ptr< ICalcAngle > alg_ptr;
        alg_ptr alg;

        switch( mClockType ) {
            case ClockType::Mechanical:
                alg = alg_ptr( new CalcAngleForMClock() );
                break;
            case ClockType::Quartz:
                alg = alg_ptr( new CalcAngleForQClock() );
                break;
        }

        mAngle.angle = alg->calculate( mTime );
    }

    Angle CalcAngleBetweenHandsModel::getAngle() const
    {
        return mAngle;
    }

    // TODO: Validator implementation
    bool Validator::timeIsValid(const std::string& rawTime, Time& output)
    {
        if ( rawTime.size() > kMaxTimeSize ) { return false; }

        mark_tag hours( 1 ), minutes( 2 ), format( 3 );

        static const sregex hm = -repeat<1, 2>( _d );
        static const sregex timeFormat = !(
                                           ( as_xpr( 'A' ) | 'P' ) >>
                                           ( as_xpr( 'M' ) )
                                           );
        static const sregex time =  ( hours= hm )   >>
                                    ":"             >>
                                    ( minutes= hm ) >>
                                    ( -!_s )        >>
                                    ( format= timeFormat )
                                    ;

        smatch what;

        bool tmpResult = regex_match( rawTime, what, time );

        if ( tmpResult ) {
            size_t tmpHours       = atoi( what[ hours   ].str().c_str() );
            size_t tmpMinutes     = atoi( what[ minutes ].str().c_str() );
            std::string tmpFormat = std::move( what[ format ].str() );

            if ( ( ( tmpFormat != "" && tmpHours <= kMaxHourValueAMPM ) ||
                   ( tmpFormat == "" && tmpHours <= kMaxHourValue24   ) ) &&
                     tmpMinutes <= kMaxMinuteValue ) {
                output.hour   = tmpHours;
                output.minute = tmpMinutes;
                output.type   = what[ format  ];
            } else {
                tmpResult = false;
            }
        }

        return tmpResult;
    }

    bool Validator::clockIsValid(const std::string& rawClock, std::string& output)
    {
        if( rawClock.size() > kMaxClockTypeSize  ) { return false; }

        bool tmpResult = ( rawClock == "MC" || rawClock == "QC" );
        if ( tmpResult ) {
                output = rawClock;
        }

        return tmpResult;
    }

    bool Validator::formatIsValid(const std::string& rawFormat, std::string& output)
    {
        if( rawFormat.size() > kMaxOutputFormatSize ) { return false; }

        bool tmpResult = ( rawFormat == "deg" || rawFormat == "rad" || rawFormat =="dms" );
        if ( tmpResult ) {
            output = rawFormat;
        }
        return tmpResult;
    }

    // TODO: CalcAngleForQClock implementation
    fnum CalcAngleForQClock::calculate(Time& time )
    {
        // hours in interval [0..11]
        fnum h = fixHour( time.hour ),
             m = time.minute;

        // h * 5 -> convert hours in "minutes" representation
        // (on dial: 1 hour - 5 minutes, 2 hours - 10 minutes, 3 hours - 15 minutes, etc.)
        // - m   -> minus current minutes value
        // fabs() -> less than zero... it's not bad, but very strange and require additional validation
        // * 6.0 -> to degrees
        fnum angle = fabs( h * 5.0 - m  ) * 6.0;

        // angle between hands
        if ( angle > 180 ) angle = 360 - angle;

        return angle;
    }

    fnum CalcAngleForQClock::fixHour(const fnum& h)
    {
        return ( h >= 12.0 ? h - 12.0 : h );
    }

     CalcAngleForQClock::~CalcAngleForQClock()
    {
    }

    // TODO: CalcAngleForMClock implementation
    fnum CalcAngleForMClock::calculate(Time& time)
    {
        // see full description in CalcAngleForQClock::calculate( Time& time )
        fnum h = fixHour( time.hour ),
             m = time.minute;

        // + m / 12 -> hour hand moves 12 times slower than the minute hand
        fnum angle = fabs( h * 5.0 + m / 12.0 - m  ) * 6.0;

        if ( angle > 180 ) angle = 360 - angle;

        return angle;
    }

     CalcAngleForMClock::~CalcAngleForMClock()
    {
    }

     ICalcAngle::~ICalcAngle()
    {
    }

    // TODO: Tables implementation
    const Tables& Tables::instance()
    {
        static Tables table;
        return table;
    }

    ClockType Tables::toClockType(const std::string& strType) const
    {
        if( mClockType.find( strType ) != mClockType.end() ) {
            return mClockType.at( strType );
        } else {
            return ClockType::Mechanical;
        }
    }

    ResultType Tables::toResultType(const std::string& strResultType) const
    {
        if( mResultType.find( strResultType ) !=  mResultType.end() ) {
            return mResultType.at( strResultType );
        } else {
            return ResultType::deg;
        }
    }

    Tables::Tables()
    {
        mClockType[ "MC" ] = ClockType::Mechanical;
        mClockType[ "QC" ] = ClockType::Quartz;

        mResultType[ "deg" ] = ResultType::deg;
        mResultType[ "rad" ] = ResultType::rad;
        mResultType[ "dms" ] = ResultType::dms;
    }

} // end of calc_angle

#if !(defined NO_MAIN_CLOCK) // for simple test, because all code in one file

// TODO: main function
int main( int argc, char** argv )
{
    if( argv[ 1 ] == nullptr ||
        argv[ 2 ] == nullptr ||
        argv[ 3 ] == nullptr ) {
        std::cout << "The program requires 3 arguments:\n"
                     "[ time(hh:mm, hh:mm AM/PM) ]\n"
                     "[ output_format(deg, rad, dms) ]\n"
                     "[ clock_type(MC, QC) ]\n\n"
                     "Example: \"10:33 PM\" deg QC" << std::endl;
        return -1;
    }

    calc_angle::CalcAngleBetweenHandsModel model;
    calc_angle::CalcAngleBetweenHandsController controller( model );

    controller.setTime( argv[ 1 ]  );
    controller.setOutputFormat( argv[ 2 ] );
    controller.setKindOfClock( argv[ 3 ] );

    controller.calculate();
    controller.displayResult();

    return 0;
}

#endif
