#define NO_MAIN_CLOCK
#include "../../main.cpp"

#include <boost/test/minimal.hpp>


int test_main( int, char *[] )
{
    // validator tests
    calc_angle::Validator v;
    std::string out_s;
    calc_angle::Time out_time;

    BOOST_CHECK( v.timeIsValid( "15:30", out_time ) );
    BOOST_CHECK( out_time.hour == 15 && out_time.minute == 30 && out_time.type == "" );
    BOOST_CHECK( v.timeIsValid( "10:25PM", out_time ) );
    BOOST_CHECK( out_time.hour == 10 && out_time.minute == 25 && out_time.type == "PM" );
    BOOST_CHECK( v.timeIsValid( "11:15 AM", out_time ) );
    BOOST_CHECK( out_time.hour == 11 && out_time.minute == 15 && out_time.type == "AM" );

    BOOST_CHECK( !v.timeIsValid( "12:00PM", out_time ) );
    BOOST_CHECK( !v.timeIsValid( "25:00", out_time ) );
    BOOST_CHECK( !v.timeIsValid( "12:99", out_time ) );
    BOOST_CHECK( !v.timeIsValid( "121:00", out_time ) );

    BOOST_CHECK( v.clockIsValid( "MC", out_s ) );
    BOOST_CHECK( out_s == "MC" );
    BOOST_CHECK( v.clockIsValid( "QC", out_s ) );
    BOOST_CHECK( out_s == "QC" );

    BOOST_CHECK( !v.clockIsValid( "ddMC", out_s ) );
    BOOST_CHECK( !v.clockIsValid( "", out_s ) );

    BOOST_CHECK( v.formatIsValid( "deg", out_s ) );
    BOOST_CHECK( out_s == "deg" );
    BOOST_CHECK( v.formatIsValid( "rad", out_s ) );
    BOOST_CHECK( out_s == "rad" );
    BOOST_CHECK( v.formatIsValid( "dms", out_s ) );
    BOOST_CHECK( out_s == "dms" );

    BOOST_CHECK( !v.formatIsValid( "radgr", out_s ) );
    BOOST_CHECK( !v.formatIsValid( "", out_s ) );

    // calc test
    calc_angle::CalcAngleForMClock m_calc;
    calc_angle::CalcAngleForQClock q_calc;
    calc_angle::Time t;
    calc_angle::Angle a;

    t.hour = 15;
    t.minute = 15;
    BOOST_CHECK( q_calc.calculate( t ) == 0 );
    BOOST_CHECK( m_calc.calculate( t ) == 7.5 );

    t.hour = 3;
    t.minute = 0;
    BOOST_CHECK( m_calc.calculate( t ) == 90 );

    t.hour = 15;
    t.minute = 0;
    a.angle = m_calc.calculate( t );
    BOOST_CHECK( a.toString( calc_angle::ResultType::rad ) == "1.5708" );

    t.hour = 9;
    t.minute = 0;
    t.type = "PM";
    a.angle = m_calc.calculate( t );
    BOOST_CHECK( a.toString( calc_angle::ResultType::dms ) == "90.0'0''" );

    return 0;
}
